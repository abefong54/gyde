import React from 'react';
//import {FaSearch} from './react-icons/lib/fa';
import search from './search.png';
import './Toolbar.css';

const toolbar = props => (
    <nav className="navbar top">
        <span className="logo"><a href="/">gyde</a></span>
        <span className="toolbar_navigation_items"><img src={search} alt=""/></span>
    </nav>
);

export default toolbar;