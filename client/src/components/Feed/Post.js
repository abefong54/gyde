import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import profile_pic from './niki.jpg';
import image from './Image 4.png';
import './Feed.css';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'

function Feed(props) {
    <>
        <body>
            <div class="card mt-3">
                <div class="card-body">
                    {/* PROFILE PIC */}
                    <div class="row ">
                        <div class="col-4">
                            <img src={profile_pic} alt="" className="border rounded profile_pic"/>
                            <span className="username">  nikizenfanya </span>
                            <span> <i> works in </i> Microsoft</span> 
                        </div>
                    </div>

                    {/* image */}
                    <div class="border shadow-lg p-3 mb-5 bg-white rounded">
                        <img src={image} alt="" className="rounded image rounded mx-auto d-block"/>
                    </div>

                    {/* TAGS */}
                    <div className="border">
                        <span class="image col-12 row ">Lorem ipsum dolor sit amet, eum an alia libris pertinacia, has dicta partem recteque cu, ea alienum principes repudiandae est. No magna epicurei mei. Eius vocent sea ad, in cum dictas omittantur. Integre ancillae quo ne, tale recusabo consulatu no ius. Eos ne eruditi delicatissimi, eam eu modus latine numquam. Ut omnis euismod argumentum usu.e</span>
                        <p class="hashtags">#fintech #healthcare #startups</p>
                    </div>

                    {/* TIMESTAMP */}
                    <div className="timestamp">
                        1 hour ago
                    </div>
                
                  </div>
            </div>
        </body>
    </>
}
export default Feed;