
import express from "express"
import mongodb from "mongodb"
import bodyParser from "body-parser"  
import path from "path"
import request from "request"

const app = express();
const MongoClient = mongodb.MongoClient;
const PORT = 4000;
const DATABASE = "gyde-db"
const uri = "mongodb+srv://nodeUser:aw5som3_dbp4$$1!@cluster0-e6ite.gcp.mongodb.net/test?retryWrites=true&w=majority";

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,"/build")));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

const withDb = async (operations) => {
    try {

        const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
        const db = client.db(DATABASE);            
        await operations(db);
        client.close();
    
    } catch (e) {
        console.log("Error connecting to db", e);
    } 
}

app.get("/", (req,res) => {
    res.send("Cool");  
})

app.get("/api/user/:firstName", async (req,res) => {
    withDb( async (db) => {
        const firstName = req.params.firstName;
        await db.collection("user").findOne({first_name:firstName})
            .then(response => res.status(200).json(response))
            .catch(error => console.error(error));
    })
});

app.post("/api/user/create", async (req,res) => {
    withDb( async (db) => {
        const newUser = req.body;
        await db.collection("user").insertOne(newUser)
            .then(response => res.status(200).json(response.ops[0]))
            .catch(error => console.error(error));
    })
});



app.post("/api/feed/create-post", async (req,res) => {
    withDb( async (db) => {
        const newPost = req.body;
        await db.collection("feed_post").insertOne(newPost)
            .then(response => res.status(200).json(response.ops))
            .catch(error => console.error(error));
    })
});


// TODO NEEDS WORK
// app.put("/api/feed/update-post/:post_id", async (req,res) => {
//     withDb( async (db) => {
//         const postID = req.post_id;
//         const oldData = { "_id": postID };
//         const newData = {
//             "$set": {
//                 "user_name" : req.body.user_name,
//                 "text" : req.body.text,
//                 "tags" : req.body.tags,
//                 "time_stamp": req.body.time_stamp,
//                 "image_path": req.body.image_path,
//                 "company": req.body.company
//             }
//         };
//         const options = { "upsert": false };

//         const updatePost = req.body;
//         await db.collection("feed_post").updateOne(oldData, newData, options)
//             .then(response => res.status(200).json(response.modifiedCount))
//             .catch(error => console.error(error));
//     })
// });


app.get("/api/feed", async (req,res) => {
    withDb( async (db) => {
        await db.collection("feed_post").find().toArray()
            .then(response => res.status(200).json(response))
            .catch(error => console.error(error));
    })
});


app.get("*", (req,res) => {

    res.sendFile(path.join(__dirname+"/build/index.html"));

})

app.listen(PORT, () => console.info(`REST API running on port ${PORT}`));


// npx babel-node index.js